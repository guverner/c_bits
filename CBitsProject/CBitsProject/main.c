#include <stdio.h>

//&		-bitwise and
//|		-bitwise or
//^		-bitwise xor
//~		-bitwise not
//<<	-bitwise shift left
//>>	-bitwise shift right

//Check if the integer is even or odd.
int isIntOdd(int x) {
	return (x & 1);
}

//is the n-th bit is set.
int isNthBitSet(int x, int n) {
	return ((x & (1 << n)) && 1);
}

//Set the n-th bit.
int setNthBit(int x, int n) {
	return x | (1 << n);
}

//unset the n-th bit.
int unsetNthBit(int x, int n) {
	return x & (~(1 << n));
}

//Toggle the n-th bit.
int toggleNthBit(int x, int n) {
	return x ^ (1 << n);
}

//Turn off the rightmost 1-bit.
int turnOffRightmost1Bit(int x) {
	return x & (x - 1);
}

//Isolate the rightmost 1-bit.
int isolateRightmost1Bit(int x) {
	return x & (-x);
}

//Right propagate the rightmost 1-bit.
int rightPropagateRightmost1Bit(int x) {
	return x | (x - 1);
}

//Isolate the rightmost 0-bit.
int isolateRightmost0Bit(int x) {
	return ~x & (x + 1);
}

//Turn on the rightmost 0-bit.
int turnOnRightmost0Bit(int x) {
	return x | (x + 1);
}

void int_to_bin(int num) {
	const int numBitsInInt = sizeof(int) * 8;
	char str[sizeof(int) * 8 + 1] = { 0 };
	int i;
	for (i = numBitsInInt - 1; i >= 0; i--) {
		str[i] = (num & 1) ? '1' : '0';
		num >>= 1;
	}
	printf("%s\n", str);
}

//Mask with least signficant n bits set to 1
//Examples: n = 6 --> Ox3F, n = 17 --> Ox1FFFF
//Assume 1 <= n <= w
int lower_one_mask(int n) {
	return (1 << n) - 1;
}

//Do rotating left shift. Assume 0 <= n < w
//Examples when x = Ox12345678 and w = 32:
//n = 4->Ox23456781, n = 20->Ox67812345
unsigned rotate_left(unsigned x, int n) {
	int numBitsInInt = sizeof(int) * 8;
	return (x << n) | ((x & (-1 << (numBitsInInt - n))) >> (numBitsInInt - n));
}

//Return 1 when x can be represented as an n-bit, 2's-complement
//number; 0 otherwise
//Assume 1 <= n <= w
int fits_bits(int x, int n) {
	return !(x & ~((1 << n) - 1));
}

//argc refers to the number of command line arguments passed in, which includes 
//the actual name of the program, as invoked by the user.argv contains the actual arguments, 
//starting with index 1. Index 0 is the program name.
int main(int argc, char *argv[]) {
	return 0;
}

